import React, { Component } from "react";

import {
  getAllChecklist,
  createChecklist,
  deleteChecklist,
} from "../api/checklist-api";

import "../style/checklist.css";
import Checklistitem from "./checkListItem";

const key = process.env.REACT_APP_TRELLO_KEY;
const token = process.env.REACT_APP_TRELLO_TOKEN;

export class CheckList extends Component {
  constructor() {
    super();
    this.state = {
      flag: true,
      checklists: null,
      name: "",
    };
    this.delChecklist = this.delChecklist.bind(this);
    this.addChecklist = this.addChecklist.bind(this);
    this.checkList = this.checkList.bind(this);
  }

  componentDidMount() {
    const cardID = this.props.cardID;
    getAllChecklist(cardID, key, token).then((checklistArr) =>
      this.setState({ checklists: checklistArr })
    );
  }

  addChecklist() {
    const cardID = this.props.cardID;
    const name = this.state.name;
    if (name) {
      createChecklist(cardID, name, key, token).then((data) => {
        this.componentDidMount();
      });
    }
    this.setState({ name: "", flag: true });
  }

  delChecklist(id) {
    deleteChecklist(id, key, token).then((res) => {
      this.componentDidMount();
    });
  }

  checkList(checklist) {
    return (
      <>
        <div className="checklistHeader">
          <h2 className="checklistName">{checklist.name}</h2>
          <p
            className="deleteCard"
            onClick={() => {
              this.delChecklist(checklist.id);
            }}
          >
            Delete
          </p>
        </div>
        <Checklistitem checklistID={checklist.id} />
      </>
    );
  }

  render() {
    return (
      <>
        <div className="checklistContainer">
          {this.state.checklists &&
            this.state.checklists.map((Checklist) => (
              <this.checkList key={Checklist.id} {...Checklist} />
            ))}
        </div>

        <div className="createList">
          {this.state.flag ? (
            <p
              className="addCard"
              onClick={() => this.setState({ flag: false })}
            >
              + Add checklist
            </p>
          ) : (
            <div className="popUp">
              <input
                type="text"
                className="popUpInput"
                placeholder="item name"
                onChange={(e) => {
                  this.setState({ name: e.target.value });
                }}
              />
              <div className="popUpBtn">
                <button className="submitBtn" onClick={this.addChecklist}>
                  Add item
                </button>
                <h3
                  className="closeBtn"
                  onClick={() => this.setState({ flag: true, name: "" })}
                >
                  x
                </h3>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default CheckList;
