export function getAllList(id, key, token) {
  return fetch(
    `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`,
    { method: "GET" }
  )
    .then((response) => response.json())
    .then((listArr) => listArr)
    .catch((err) => {
      console.log(err);
    });
}

export function createList(id, name, key, token) {
  return fetch(
    `https://api.trello.com/1/lists?key=${key}&token=${token}&name=${name}&idBoard=${id}`,
    { method: "POST" }
  )
    .then((response) => response.json())
    .then((listInfo) => listInfo)
    .catch((err) => {
      console.log(err);
    });
}

export function deleteList(id, key, token) {
  return fetch(
    `https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`,
    { method: "PUT" }
  )
    .then((response) => response.json())
    .catch((err) => {
      console.log(err);
    });
}
