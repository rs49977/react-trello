import React, { Component } from "react";
import Checklist from "./checkList";

import "../style/cardPopUp.css";

export class CardPopUp extends Component {
  render() {
    if (this.props.check)
      return (
        <div className="containerWrapper">
          <div className="container">
            <div className="containerHeader">
              <h2 className="containerName">{this.props.card.name}</h2>
              <h2 className="containerClose" onClick={this.props.handler}>
                x
              </h2>
            </div>
            <div className="containerBody">
              <Checklist cardID={this.props.card.id} />
            </div>
          </div>
        </div>
      );
    return null;
  }
}

export default CardPopUp;
