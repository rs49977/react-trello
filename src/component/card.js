import React, { Component } from "react";
import { getAllCard, createCard, deleteCard } from "../api/card-api";

import "../style/card.css";

import CardPopUp from "./cardPopUp";

const key = process.env.REACT_APP_TRELLO_KEY;
const token = process.env.REACT_APP_TRELLO_TOKEN;

export class Card extends Component {
  constructor() {
    super();

    this.state = {
      cards: null,
      flag: true,
      name: "",
      check: false,
      cardInfo: null,
    };

    this.addCard = this.addCard.bind(this);
    this.delCard = this.delCard.bind(this);
    this.card = this.card.bind(this);
    this.setCheck = this.setCheck.bind(this);
    this.changeCheck = this.changeCheck.bind(this);
  }

  setCheck(card) {
    this.setState({ check: !this.state.check, cardInfo: card });
  }

  changeCheck() {
    this.setState({ check: !this.state.check });
  }

  componentDidMount() {
    const listID = this.props.listID;
    getAllCard(listID, key, token).then((cardArr) =>
      this.setState({ cards: cardArr })
    );
  }

  delCard(id) {
    deleteCard(id, key, token).then(() => {
      const newCards = this.state.cards.filter((card) => card.id !== id);
      this.setState({ cards: newCards });
    });
  }

  card(card) {
    return (
      <>
        <CardPopUp
          card={this.state.cardInfo}
          check={this.state.check}
          handler={this.changeCheck}
        />

        <div className="card" onClick={() => this.setCheck(card)}>
          <h4 className="cardName">{card.name}</h4>
          <p className="deleteCard" onClick={() => this.delCard(card.id)}>
            Delete
          </p>
        </div>
      </>
    );
  }

  addCard(e) {
    const name = this.state.name;
    if (name) {
      createCard(this.props.listID, this.state.name, key, token).then((card) =>
        this.setState({
          cards: [...this.state.cards, card],
          name: "",
          flag: true,
        })
      );
    }
  }

  render() {
    return (
      <>
        {this.state.cards &&
          this.state.cards.map((card) => <this.card key={card.id} {...card} />)}

        <div className="createList">
          {this.state.flag ? (
            <p
              className="addCard"
              onClick={() => this.setState({ flag: false })}
            >
              + Add another list
            </p>
          ) : (
            <div className="popUp">
              <input
                type="text"
                className="popUpInput"
                placeholder="Enter card name"
                onChange={(e) => {
                  this.setState({ name: e.target.value });
                }}
              />
              <div className="popUpBtn">
                <button className="submitBtn" onClick={this.addCard}>
                  Add item
                </button>
                <h3
                  className="closeBtn"
                  onClick={() => this.setState({ flag: true, name: "" })}
                >
                  x
                </h3>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default Card;
