import React, { Component } from "react";
import { getBoard } from "../api/board-api";
import { getAllList, createList, deleteList } from "../api/list-api";

import Card from "./card";

import "../style/list.css";

const key = process.env.REACT_APP_TRELLO_KEY;
const token = process.env.REACT_APP_TRELLO_TOKEN;

export class List extends Component {
  constructor() {
    super();

    this.state = {
      board: null,
      flag: true,
      lists: null,
      name: "",
    };

    this.setValue = this.setValue.bind(this);
    this.addList = this.addList.bind(this);
    this.delList = this.delList.bind(this);
    this.list = this.list.bind(this);
  }

  componentDidMount() {
    console.log(this.state.name);

    const boardId = this.props.match.params.id;
    getBoard(boardId, key, token).then((boardInfo) =>
      this.setState({
        board: boardInfo,
      })
    );

    getAllList(boardId, key, token).then((listArr) => {
      this.setState({
        lists: listArr,
      });
    });
  }

  setValue(e) {
    this.setState({ name: e.target.value });
  }

  async addList(e) {
    if (this.state.name) {
      const name = this.state.name;
      const newList = await createList(this.state.board.id, name, key, token);
      this.setState({ lists: [...this.state.lists, newList] });
      this.setState({ flag: true, name: "" });
    }
  }

  delList(id) {
    deleteList(id, key, token).then(() => {
      const newList = this.state.lists.filter((list) => list.id !== id);
      this.setState({ lists: newList });
    });
  }

  list(list) {
    return (
      <div className="cardContainer">
        <div className="listHeader">
          <h3 className="listName">{list.name}</h3>
          <p className="deleteList" onClick={() => this.delList(list.id)}>
            Delete
          </p>
        </div>
        <Card listID={list.id} />
      </div>
    );
  }

  render() {
    if (this.state.board) {
      document.querySelector(
        ".App"
      ).style.backgroundColor = this.state.board.prefs.backgroundColor;
      document.querySelector(".App").style.backgroundImage = `url(
      ${this.state.board.prefs.backgroundImage}
    )`;
      document.querySelector(".App").style.backgroundSize = "100% 100%";
    }

    return (
      <section className="listWrapper">
        {this.state.board && (
          <h1 className="boardName">{this.state.board.name}</h1>
        )}

        <div className="listContainer">
          {this.state.lists &&
            this.state.lists.map((listInfo) => (
              <this.list key={listInfo.id} {...listInfo} />
            ))}

          <div className="createList">
            {this.state.flag ? (
              <p
                className="addList"
                onClick={() => this.setState({ flag: false })}
              >
                + Add another list
              </p>
            ) : (
              <div className="popUp">
                <input
                  type="text"
                  className="popUpInput"
                  placeholder="Enter list name"
                  onChange={this.setValue}
                />
                <div className="popUpBtn">
                  <button className="submitBtn" onClick={this.addList}>
                    Add item
                  </button>
                  <h3
                    className="closeBtn"
                    onClick={() => this.setState({ flag: true, name: "" })}
                  >
                    x
                  </h3>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    );
  }
}

export default List;
