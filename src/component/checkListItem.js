import React, { Component } from "react";

import {
  createChecklistItem,
  deleteChecklistItem,
  getAllChecklistItem,
} from "../api/checklistitem-api";
import "../style/checklistitem.css";

const key = process.env.REACT_APP_TRELLO_KEY;
const token = process.env.REACT_APP_TRELLO_TOKEN;

export class CheckListItem extends Component {
  constructor() {
    super();
    this.state = {
      flag: true,
      checklistitems: null,
      name: "",
    };
    this.delChecklistitem = this.delChecklistitem.bind(this);
    this.addChecklistitem = this.addChecklistitem.bind(this);
    this.checkListItem = this.checkListItem.bind(this);
  }

  componentDidMount() {
    const checklistID = this.props.checklistID;
    getAllChecklistItem(checklistID, key, token).then((checklistitemArr) =>
      this.setState({ checklistitems: checklistitemArr })
    );
  }

  addChecklistitem() {
    const checklistID = this.props.checklistID;
    const name = this.state.name;
    if (name) {
      createChecklistItem(checklistID, name, key, token).then((data) => {
        this.componentDidMount();
      });
    }
    this.setState({ name: "", flag: true });
  }

  delChecklistitem(id) {
    const checklistID = this.props.checklistID;
    deleteChecklistItem(checklistID, id, key, token).then((res) => {
      this.componentDidMount();
    });
  }

  checkListItem(checklistitem) {
    return (
      <div className="checklistHeader">
        <p className="checklistName">{checklistitem.name}</p>
        <p
          className="deleteCard"
          onClick={() => {
            this.delChecklistitem(checklistitem.id);
          }}
        >
          Delete
        </p>
      </div>
    );
  }

  render() {
    return (
      <>
        <div className="checklistitemContainer">
          {this.state.checklistitems &&
            this.state.checklistitems.map((Checklistitem) => (
              <this.checkListItem key={Checklistitem.id} {...Checklistitem} />
            ))}
        </div>

        <div className="createList">
          {this.state.flag ? (
            <p
              className="addCard"
              onClick={() => this.setState({ flag: false })}
            >
              + Add checklist
            </p>
          ) : (
            <div className="popUp">
              <input
                type="text"
                className="popUpInput"
                placeholder="item name"
                onChange={(e) => {
                  this.setState({ name: e.target.value });
                }}
              />
              <div className="popUpBtn">
                <button className="submitBtn" onClick={this.addChecklistitem}>
                  Add item
                </button>
                <h3
                  className="closeBtn"
                  onClick={() => this.setState({ flag: true, name: "" })}
                >
                  x
                </h3>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default CheckListItem;
