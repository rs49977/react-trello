export function getAllBoard(key, token) {
  return fetch(
    `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`
  )
    .then((response) => response.json())
    .then((boards) => boards);
}

export function createBoard(name, key, token) {
  return fetch(
    `https://api.trello.com/1/boards?key=${key}&token=${token}&name=${name}`,
    {
      method: "POST",
    }
  )
    .then((response) => response.json())
    .then((board) => board);
}

export function deleteBoard(id, key, token) {
  return fetch(
    `https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  )
    .then((response) => response.json())
    .then((board) => board);
}

export function getBoard(id, key, token) {
  return fetch(
    `https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`
  )
    .then((response) => response.json())
    .then((board) => board);
}
