export function getAllChecklist(id, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`
  )
    .then((response) => response.json())
    .then((checklistArr) => checklistArr);
}

export function createChecklist(id, name, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}&name=${name}`,
    { method: "POST" }
  )
    .then((response) => response.json())
    .then((checklist) => checklist);
}

export function deleteChecklist(id, key, token) {
  return fetch(
    `https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  ).then((response) => response.json());
}
