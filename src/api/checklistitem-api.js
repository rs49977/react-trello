export function getAllChecklistItem(id, key, token) {
  return fetch(
    `https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`
  )
    .then((response) => response.json())
    .then((checklistItemArr) => checklistItemArr);
}

export function createChecklistItem(id, name, key, token) {
  return fetch(
    `https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}&name=${name}`,
    { method: "POST" }
  )
    .then((response) => response.json())
    .then((checklistItemInfo) => checklistItemInfo);
}

export function deleteChecklistItem(id, cId, key, token) {
  return fetch(
    `https://api.trello.com/1/checklists/${id}/checkItems/${cId}?key=${key}&token=${token}`,
    { method: "DELETE" }
  );
}
