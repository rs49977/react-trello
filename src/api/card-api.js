export function getAllCard(id, key, token) {
  return fetch(
    `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`
  )
    .then((response) => response.json())
    .then((cardArr) => cardArr)
    .catch((err) => {
      console.log(err);
    });
}

export function createCard(id, name, key, token) {
  return fetch(
    `https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${id}&name=${name}`,
    { method: "POST" }
  )
    .then((response) => response.json())
    .then((cardInfo) => cardInfo)
    .catch((err) => {
      console.log(err);
    });
}

export function deleteCard(id, key, token) {
  return fetch(
    `https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`,
    {
      method: "DELETE",
    }
  ).then((response) => response.json());
}
