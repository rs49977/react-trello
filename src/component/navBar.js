import React from "react";

import "../style/navBar.css";

function NavBar() {
  return (
    <div className="navBar">
      <span className="homeBtn" role="img" aria-label="HouseIcon">
        <svg
          width="24"
          height="24"
          role="presentation"
          focusable="false"
          viewBox="0 0 24 24"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M3.58579 10.4142C3.21071 10.7893 3 11.298 3 11.8284V19C3 20.1045 3.89543 21 5 21H10C10.5523 21 11 20.5523 11 20V14H13V20C13 20.5523 13.4477 21 14 21H19C20.1046 21 21 20.1045 21 19V11.8284C21 11.298 20.7893 10.7893 20.4142 10.4142L12.7071 2.70708C12.3166 2.31655 11.6834 2.31655 11.2929 2.70708L3.58579 10.4142ZM13 12C14.1046 12 15 12.8954 15 14V19H19V11.8284L12 4.8284L5 11.8284V19H9V14C9 12.8954 9.89543 12 11 12H13Z"
            fill="white"
          ></path>
        </svg>
      </span>

      <span
        style={{
          height: "20px",
          width: "100px",
          backgroundImage: `url(https://a.trellocdn.com/prgb/dist/images/header-logo-spirit.d947df93bc055849898e.gif)`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "contain",
        }}
      ></span>

      <span
        style={{
          height: "32px",
          width: "32px",
          backgroundImage: `url("https://trello-members.s3.amazonaws.com/605c4b8d627d5f731a1b7d15/45532814ab22f19b09f3dbfc681c84ec/170.png")`,
          backgroundSize: "contain",
        }}
      ></span>
    </div>
  );
}

export default NavBar;
