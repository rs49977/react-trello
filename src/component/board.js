import React, { Component } from "react";
import { getAllBoard, deleteBoard, createBoard } from "../api/board-api";
import { Link } from "react-router-dom";

import "../style/board.css";

const key = process.env.REACT_APP_TRELLO_KEY;
const token = process.env.REACT_APP_TRELLO_TOKEN;

export class Board extends Component {
  constructor() {
    super();
    this.state = {
      boards: null,
      popUP: false,
      value: "",
    };

    this.changePopUp = this.changePopUp.bind(this);
    this.PopUp = this.PopUp.bind(this);
    this.createBoard = this.createBoard.bind(this);
    this.delBoard = this.delBoard.bind(this);
    this.Board = this.Board.bind(this);
    this.setValue = this.setValue.bind(this);
  }

  componentDidMount() {
    getAllBoard(key, token)
      .then((boardsArr) => {
        this.setState({
          boards: boardsArr,
        });
      })
      .catch((err) => console.log(err));
    document.querySelector(".App").style.backgroundColor = "white";
  }

  createBoard() {
    if (this.state.value) {
      createBoard(this.state.value, key, token).then((res) => {
        this.componentDidMount();
      });
    }
    this.setState({ value: "", popUP: false });
  }

  async delBoard(id) {
    await deleteBoard(id, key, token);
    const newBoards = this.state.boards.filter((board) => board.id !== id);
    this.setState({ boards: newBoards });
  }

  changePopUp() {
    this.setState({ popUP: !this.state.popUP });
  }

  setValue(e) {
    this.setState({ value: e.target.value });
  }

  Board(board) {
    return (
      <div
        className="board"
        style={{
          backgroundColor: `${board.prefs.backgroundColor}`,
          backgroundImage: `url(${board.prefs.backgroundImage})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "100% 100%",
        }}
      >
        <h3>{board.name}</h3>
        <p className="deleteBoard">Delete</p>
      </div>
    );
  }

  PopUp() {
    return (
      <div className="pop-up-container">
        <div className="pop-up-menu">
          <input
            type="text"
            placeholder="Add board id"
            onChange={this.setValue}
          />
          <div className="pop-up-close-btn" onClick={this.changePopUp}>
            x
          </div>
        </div>
        <button onClick={this.createBoard}>Create board</button>
      </div>
    );
  }

  render() {
    return (
      <>
        {this.state.popUP && <this.PopUp />}

        <div className="boardContainer">
          {this.state.boards &&
            this.state.boards.map((board) => (
              <Link
                to={`/board/${board.id}`}
                style={{ textDecoration: "none" }}
                onClick={(e) => {
                  if (e.target.classList.contains("deleteBoard")) {
                    e.preventDefault();
                    this.delBoard(board.id);
                  }
                }}
                key={board.id}
              >
                <this.Board {...board} />
              </Link>
            ))}

          <div className="board createBoard" onClick={this.changePopUp}>
            <p onClick={this.openPopUp}>Create a new board</p>
          </div>
        </div>
      </>
    );
  }
}

export default Board;
