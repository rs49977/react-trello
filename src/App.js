import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./style/App.css";
import NavBar from "./component/navBar";
import Board from "./component/board";
import List from "./component/list";

export class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />
          <Switch>
            <Route exact path="/" component={Board} />
            <Route path="/board/:id" component={List} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
